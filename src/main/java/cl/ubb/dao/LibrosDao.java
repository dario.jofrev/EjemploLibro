package cl.ubb.dao;

import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Libro;
public interface LibrosDao extends CrudRepository<Libro, Long>{

}
