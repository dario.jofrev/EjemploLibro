package cl.ubb.ejemploLibros;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import cl.ubb.dao.LibrosDao;
import cl.ubb.model.Libro;
import cl.ubb.service.LibrosService;

@RunWith(MockitoJUnitRunner.class)

public class LibrosServiceTest {

	@Mock
	private LibrosDao librosDao;
	
	@InjectMocks
	private LibrosService librosService;
	
	@Test
	public void deboCrearLibroYRetornarlo(){
		Libro libro = new Libro();
		libro.setId(1L);
		
		when(librosDao.save(libro)).thenReturn(libro);
		//Libro libroCreado = librosService.
	}

}
